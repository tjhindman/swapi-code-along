// @ts-nocheck
import React from "react";
import { makeRequest } from "./utils";
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import { withStyles } from "@material-ui/core/styles";

const styles = (theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
});

class FilmItem extends React.Component {
  state = {
    film: null,
  };

  componentDidMount() {
    this.loadCurrentFilm();
  }

  async loadCurrentFilm() {
    try {
      const film = await makeRequest(this.props.match.url);
      this.setState({ film });
    } catch {
      // Todo error check this =]
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.filmID !== prevProps.match.params.filmID) {
      this.loadCurrentFilm();
    }
  }

  render() {
    const { film } = this.state;
    const { classes } = this.props;

    if (!film) {
      return null;
    }
    return (
      <div>
        <Card className={classes.root}>
          <CardHeader
            title={film.title}
            subheader={film.release_date}
          />
          <CardMedia
            className={classes.media}
            image={film.image ? film.image : require("./images/SWL.png")}
            title={film.title}
          />
          <CardContent>
            <Typography variant="body2" color="textSecondary" component="p">
              {film.opening_crawl}
            </Typography>
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withStyles(styles)(FilmItem)