// @ts-nocheck
import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import { connect } from 'react-redux';
import Login from './Login'
import { logout } from './actions';
import Loading from "./Loading";
import Films from "./Films";
import { makeRequest } from "./utils";

class App extends React.Component {
  state = {
    loading: true,
    navItems: [],
    sidebarItems: [],
  };

  componentDidMount() {
    this.fetchNavbarItems();
  }

  fetchNavbarItems = async () => {
    const res = await makeRequest("/");
    this.setState((currentState) => ({
      ...currentState,
      navItems: Object.keys(res),
      loading: false,
    }));
  };

  fetchSideItemsFor = async (item) => {
    const res = await makeRequest(`/${item}`);
    this.setState((currentState) => ({
      ...currentState,
      sidebarItems: res.results,
    }));
  };

  render() {
    const { loading, navItems, sidebarItems } = this.state;
    if (loading) {
      return <Loading />;
    }

    if (!this.props.isAuthenticated) {
      return <Login />
    }

    return (
      <>
        <nav>
          <ul>
            {navItems.map((si) => (
              <Link key={si} to={`/${si}`} onClick={() => this.fetchSideItemsFor(si)}>
                {si}
              </Link>
            ))}
            <Link to='/' onClick={this.props.logout}>
              Logout
            </Link>
          </ul>
        </nav>
        <div>
          <ul>
            {sidebarItems.map((si) => (
              <li key={`si-${si.title || si.name}`}>
                {si.title || si.name}
              </li>
            ))}
          </ul>
        </div>

        <Switch>
          <Route path="/films" component={Films} />
        </Switch>
      </>
    );
  }
}

const mapStateToProps = ({ isAuthenticated }) => ({
  isAuthenticated: isAuthenticated
})

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout())
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
