import React from 'react';
import { login } from './actions';
import { connect } from 'react-redux';
 
class Login extends React.Component {
    state = {
        email: "",
        password: ""
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    validateEmail = (e) => {
        e.preventDefault()

        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(String(this.state.email).toLowerCase())){
            this.props.login()
        };
    }

    render() { 
        return (
            <>
                <h1>Log in!</h1>
                <img src={require("./images/SWL.png")} />
                <form onSubmit={this.validateEmail} >
                    <input type="text" name="email" onChange={this.onChange} />
                    <input type="password" name="password" onChange={this.onChange} />
                    <input type="submit" value="Login" />
                </form>
            </>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    login: (email) => dispatch(login(email))
})
 
export default connect(null, mapDispatchToProps)(Login);