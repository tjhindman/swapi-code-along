import { LOGIN, LOGOUT } from '../actions';

const initialState = {
    id: 1,
    email: "",
    isAuthenticated: false
}

export const reducer = (state = initialState, action) => {
    switch(action.type) {
        case LOGIN:
            return {
            ...state,
            email: action.payload,
            isAuthenticated: !state.isAuthenticated
        }
        case LOGOUT:
            return {
                ...state,
                email: "",
                isAuthenticated: false
            }
        default:
            return state
    }
}